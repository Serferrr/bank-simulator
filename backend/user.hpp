#ifndef BANK_SIMULATOR_USER_HPP
#define BANK_SIMULATOR_USER_HPP
#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
namespace banksim {
struct Transaction {

  std::string_view m_sender_id;
  std::string_view m_reciever_id;
  int m_amount;
  std::chrono::time_point<std::chrono::system_clock> m_time_of_realisation{
      std::chrono::system_clock::now()};
  Transaction(
      std::pair<std::string_view, std::string_view> const &sender_reciever,
      int amount)
      : m_sender_id(sender_reciever.first),
        m_reciever_id(sender_reciever.second), m_amount(amount) {}
};

struct Transfer {
  enum struct type : bool { payment, withdrawal };
  int m_amount;
  type m_transaction_type;
  std::chrono::time_point<std::chrono::system_clock> m_time_of_realisation{
      std::chrono::system_clock::now()};
};

class User {
  // number of significant digits used for unique user number
  static constexpr std::uint8_t NUM_OF_SIGNIFICANT_DIGITS = 8U;

public:
  std::string m_first_name;
  std::string m_last_name;
  std::string m_unique_id;
  unsigned int m_user_number;
  int m_balance = 0;
  std::vector<Transaction> m_transaction_history{};
  std::vector<Transfer> m_transfer_history{};

  User(std::string f_name, std::string l_name, unsigned int no_user)
      : m_first_name(std::move(f_name)), m_last_name(std::move(l_name)),
        m_user_number(no_user) {
    m_unique_id = generate_unique_id(m_first_name, m_last_name, m_user_number);
  }
  [[nodiscard]] auto get_unique_id() const -> std::string {
    return m_unique_id;
  }
  static constexpr auto generate_unique_id = [](std::string const &f_name,
                                                std::string const &l_name,
                                                unsigned int u_number) {
    return std::to_string(std::hash<std::string>{}(f_name + l_name +
                                                   std::to_string(u_number)))
        .substr(0, NUM_OF_SIGNIFICANT_DIGITS);
  };
  [[nodiscard]] auto get_balance() const -> int { return m_balance; }
  auto set_balance(int change) -> void { m_balance += change; }

  auto add_to_transaction_history(Transaction const &transaction) -> void {
    m_transaction_history.push_back(transaction);
  }
  auto add_to_transfer_history(Transfer const &transfer) -> void {
    m_transfer_history.push_back(transfer);
  }
};

}; // namespace banksim
#endif
