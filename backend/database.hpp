#ifndef BANK_SIMULATOR_DATABASE_HPP
#define BANK_SIMULATOR_DATABASE_HPP

#include "user.hpp"
#include <unordered_map>
#include <utility>
namespace banksim {
class Database final {
  struct StringHash final {

    using is_transparent = void; // enables heterogenous lookup

    auto operator()(std::string_view str_v) const -> std::size_t {
      std::hash<std::string_view> const hasher;
      return hasher(str_v);
    }
  };
  std::unordered_map<std::string, User, StringHash, std::equal_to<>>
      m_database{};
  unsigned int m_num_of_unique_users = 0;
  std::string m_current_user;

public:
  auto operator=(Database const &) -> Database & = delete;
  auto operator=(Database &&) -> Database & = delete;
  Database(Database &) = delete;
  Database(Database &&) = delete;
  Database() = default;
  ~Database() = default;

  [[nodiscard]] auto get_unique_id(std::string const &f_name,
                                   std::string const &l_name,
                                   unsigned int unique_number) const
      -> std::string;
  [[nodiscard]] auto add_user(std::string const &f_name,
                              std::string const &l_name,
                              unsigned int unique_number) -> bool;
  [[nodiscard]] auto delete_user(std::string const &unique_id) -> bool;
  [[nodiscard]] auto
  make_transaction(std::pair<std::string, std::string> const &sender_reciever,
                   int amount) -> bool;

  [[nodiscard]] auto make_transfer(int amount, Transfer::type transfer_type)
      -> bool;
  [[nodiscard]] auto get_current_user() const -> std::string {
    return m_current_user;
  }
  [[nodiscard]] auto current_user_data(std::string const &current_user) const
      -> banksim::User {
    return m_database.at(current_user);
  }
};
}; // namespace banksim
#endif
