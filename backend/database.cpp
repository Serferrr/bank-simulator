#include "database.hpp"

[[nodiscard]] auto banksim::Database::get_unique_id(
    std::string const &f_name, std::string const &l_name,
    unsigned int unique_number) const -> std::string {

  if (std::string unique_id =
          User::generate_unique_id(f_name, l_name, unique_number);
      m_database.contains(unique_id)) {
    return unique_id;
  }
  return "";
}

[[nodiscard]] auto banksim::Database::add_user(std::string const &f_name,
                                               std::string const &l_name,
                                               unsigned int unique_number)
    -> bool {
  User temp_user(f_name, l_name, unique_number);
  auto [_, success] = m_database.insert_or_assign(temp_user.get_unique_id(),
                                                  std::move(temp_user));
  success ? m_num_of_unique_users++ : 0;
  return !success;
}

[[nodiscard]] auto banksim::Database::delete_user(const std::string &unique_id)
    -> bool {
  if (m_database.contains(unique_id)) {
    m_database.erase(unique_id);
    return false;
  }
  return true;
}

[[nodiscard]] auto banksim::Database::make_transaction(
    std::pair<std::string, std::string> const &sender_reciever, int amount)
    -> bool {
  auto const &[sender, reciever] = sender_reciever;
  const Transaction transaction{sender_reciever, amount};
  if (m_database.at(sender).get_balance() + (transaction.m_amount * -1) < 0) {
    /// ERROR!
    return true;
  }
  m_database.at(sender).set_balance(transaction.m_amount * -1);
  m_database.at(sender).add_to_transaction_history(transaction);

  m_database.at(reciever).set_balance(transaction.m_amount);
  m_database.at(reciever).add_to_transaction_history(transaction);
  return false;
}

auto banksim::Database::make_transfer(int amount, Transfer::type transfer_type)
    -> bool {

  const Transfer transfer{amount, transfer_type};
  if (transfer.m_transaction_type == Transfer::type::withdrawal) {
    if ((m_database.at(m_current_user).get_balance() +
         (transfer.m_amount * -1)) < 0) {
      // ERROR!!!!
      return true;
    }
    m_database.at(m_current_user).set_balance(transfer.m_amount * -1);
    m_database.at(m_current_user).add_to_transfer_history(transfer);
    return false;
  }
  m_database.at(m_current_user).set_balance(transfer.m_amount);
  m_database.at(m_current_user).add_to_transfer_history(transfer);
  return false;
}
