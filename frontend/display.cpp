#include "display.hpp"
#include <stdexcept>
#include <utility>

Display::Display() {
  initscr();
  start_color();
  cbreak();
  noecho();
  keypad(stdscr, true);
  getbegyx(stdscr, begin_yx.first, begin_yx.second);
  getmaxyx(stdscr, max_yx.first, max_yx.second);
  window_data test{.size = {max_yx.second, max_yx.first},
                   .position = {begin_yx.second, begin_yx.first},
                   .color_pair = {COLOR_WHITE, COLOR_BLUE},
                   .border = true,
                   .text = "test!"};
  windows.emplace_back(test);
  mousemask(ALL_MOUSE_EVENTS, nullptr);
  curs_set(0);
}

auto Display::refresh_screens() const -> void {
  refresh();
  for (auto const &window : windows) {
    wnoutrefresh(window.winptr.get());
  }
  doupdate();
}

Display::~Display() { endwin(); };
