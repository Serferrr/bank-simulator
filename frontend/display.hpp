#include "window.hpp"
#include <memory>
#include <ncurses.h>
#include <vector>

class Display final {

public:
  Display();
  ~Display();
  Display(Display const &) = delete;
  Display(Display &&) = delete;
  auto operator=(Display const &) -> Display & = delete;
  auto operator=(Display &&) -> Display && = delete;
  auto refresh_screens() const -> void;

private:
  std::pair<int, int> begin_yx;
  std::pair<int, int> max_yx;
  std::vector<window> windows;
};