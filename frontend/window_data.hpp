#ifndef WINDOW_DATA_HPP
#define WINDOW_DATA_HPP
#include <cstdint>
#include <optional>
#include <string_view>
#include <utility>
struct window_data {
  std::pair<int, int> size;
  std::pair<int, int> position;
  std::pair<std::int16_t, std::int16_t> color_pair;
  bool border{};
  std::optional<std::string_view> text;
};

#endif // WINDOW_DATA_HPP