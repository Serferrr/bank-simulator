#include "ncurses.h"
#include "panel.h"
#include "window_data.hpp"
#include <cstdint>
#include <memory>
#include <utility>
class window {
  inline static std::int16_t color_pair = 1;

  std::pair<int, int> m_size;
  std::pair<int, int> m_position;

public:
  std::unique_ptr<WINDOW, decltype(&delwin)> winptr;
  window(std::pair<int, int> position, std::pair<int, int> size,
         std::pair<std::int16_t, std::int16_t> color_pair_in,
         bool border = true)
      : m_size(size), m_position(position),
        winptr(newwin(m_size.second, m_size.first, m_position.second,
                      m_position.first),
               &delwin) {
    init_pair(color_pair, color_pair_in.first, color_pair_in.second);
    wbkgd(winptr.get(), COLOR_PAIR(color_pair));
    color_pair++;
    if (border) {
      box(winptr.get(), 0, 0);
    }
  }
  explicit window(const window_data &data)
      : m_size(data.size), m_position(data.position),
        winptr(newwin(m_size.second, m_size.first, m_position.second,
                      m_position.first),
               &delwin) {
    init_pair(color_pair, data.color_pair.first, data.color_pair.second);
    wbkgd(winptr.get(), COLOR_PAIR(color_pair));
    color_pair++;
    if (data.border) {
      box(winptr.get(), 0, 0);
    }
    if (data.text.has_value()) {
      mvwprintw(winptr.get(), m_size.second / 2,
                (m_size.first / 2) - data.text.value().length() / 2, "%s",
                data.text.value().data());
    }
  }
};