#include "backend/database.hpp"
#include "frontend/display.hpp"
#include <iostream>

auto main() -> int {
  // the singleton of display is created here because it may throw
  Display display;
  display.refresh_screens();
  getch();
  return EXIT_SUCCESS;
}
